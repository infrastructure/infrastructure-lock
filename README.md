# 分布式锁使用教程

## 动态配置变更

> 见center-config文档

## 依赖

```pom  
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-data-redis</artifactId>
        </dependency>
        <dependency>
            <groupId>org.redisson</groupId>
            <artifactId>redisson</artifactId>
            <version>3.29.0</version>
        </dependency>
        <dependency>
            <groupId>com.poizon.infrastructure</groupId>
            <artifactId>infrastructure-lock</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>

```

## 分布锁配置

```yaml
poizon:
  lock:
    impl:
      type: Redisson
    redis:
      redisson:
        ip: 127.0.0.1
        port: 6379
        db: 0
    # @see com.poizon.infrastructure.lock.domain.value.constant.LockConstants
    retryCount: 5
    timeout: 5
    waitTime: 3
    prefix: 'lock:'
    timeUnit: SECONDS

```