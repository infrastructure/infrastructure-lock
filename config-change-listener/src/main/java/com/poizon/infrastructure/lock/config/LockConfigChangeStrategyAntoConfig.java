package com.poizon.infrastructure.lock.config;

import com.poizon.infrastructure.lock.config.change.listener.LockConfigChangeStrategy;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

@Slf4j
public class LockConfigChangeStrategyAntoConfig {
    @Bean
    public LockConfigChangeStrategy lockConfigChangeStrategy() {
        log.info(">初始化分布锁-配置变更监听策略");
        return new LockConfigChangeStrategy();
    }
}
