package com.poizon.infrastructure.lock.config.change.listener;

import cn.hutool.core.util.StrUtil;
import com.poizon.infrastructure.center.config.core.event.listener.ConfigChangeListenerImpl;
import com.poizon.infrastructure.center.config.core.event.strategy.ConfigChangeEventStrategy;
import com.poizon.infrastructure.lock.core.enums.LockConstants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;

import java.util.Properties;
import java.util.concurrent.TimeUnit;


@Slf4j
@ConditionalOnClass(value = {ConfigChangeEventStrategy.class, ConfigChangeListenerImpl.class})
@ConditionalOnBean(value = ConfigChangeListenerImpl.class)
public class LockConfigChangeStrategy implements ConfigChangeEventStrategy {
    @Override
    public String getChangedProperty(Properties properties) {
        return null;
    }

    @Override
    public Boolean support(Properties properties) {
        String dbTypeEnum = properties.getProperty(LockConstants.POIZON_LOCK_LOCK_DB_TYPE_ENUM);
        String timeUnit = properties.getProperty(LockConstants.POIZON_LOCK_TIME_UNIT);
        String retryCount = properties.getProperty(LockConstants.POIZON_LOCK_RETRY_COUNT);
        String waitTime = properties.getProperty(LockConstants.POIZON_LOCK_WAIT_TIME);
        String timeout = properties.getProperty(LockConstants.POIZON_LOCK_TIMEOUT);
        String prefix = properties.getProperty(LockConstants.POIZON_LOCK_PREFIX);
        return StrUtil.isNotBlank(dbTypeEnum) || StrUtil.isNotBlank(timeUnit) || StrUtil.isNotBlank(retryCount) || StrUtil.isNotBlank(waitTime) || StrUtil.isNotBlank(timeout) || StrUtil.isNotBlank(prefix);
    }

    @Override
    public void configChange(Properties properties) {
        String dbTypeEnum = properties.getProperty(LockConstants.POIZON_LOCK_LOCK_DB_TYPE_ENUM);
        String timeUnit = properties.getProperty(LockConstants.POIZON_LOCK_TIME_UNIT);
        String retryCount = properties.getProperty(LockConstants.POIZON_LOCK_RETRY_COUNT);
        String waitTime = properties.getProperty(LockConstants.POIZON_LOCK_WAIT_TIME);
        String timeout = properties.getProperty(LockConstants.POIZON_LOCK_TIMEOUT);
        String prefix = properties.getProperty(LockConstants.POIZON_LOCK_PREFIX);

        if (StrUtil.isNotBlank(dbTypeEnum)) {
            LockConstants.lockDbTypeEnum = dbTypeEnum;
        }

        if (StrUtil.isNotBlank(timeUnit)) {
            LockConstants.DEFAULT_TIME_UNIT = TimeUnit.valueOf(timeUnit);
        }

        if (StrUtil.isNotBlank(retryCount)) {
            LockConstants.DEFAULT_RETRY_COUNT = Integer.parseInt(retryCount);
        }
        if (StrUtil.isNotBlank(waitTime)) {
            LockConstants.DEFAULT_WAIT_TIME = Long.parseLong(waitTime);
        }
        if (StrUtil.isNotBlank(timeout)) {
            LockConstants.DEFAULT_SERVICE_LOCK_TIMEOUT = Long.parseLong(timeout);
        }
        if (StrUtil.isNotBlank(prefix)) {
            LockConstants.LOCK_NAME_PREFIX = prefix;
        }

        log.info("lock config change: dbTypeEnum:{},timeUnit:{},retryCount:{},waitTime:{},timeout:{},prefix:{}", dbTypeEnum, timeUnit, retryCount, waitTime, timeout, prefix);
    }
}
