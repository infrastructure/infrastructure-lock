package com.test;

import com.test.service.TestService;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.TimeUnit;

@SpringBootTest(classes = Main.class)
@RunWith(SpringRunner.class)
public class Test {
    @Autowired
    private TestService testService;

    @org.junit.Test
    public void test01() throws Exception {
        testService.test02(
                "t",
                10L,
                10L
                , TimeUnit.SECONDS
        );
    }


}
