package com.test.service;

import com.poizon.infrastructure.lock.core.annoations.Lock;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Service
@Slf4j
@RestController
@RequestMapping("/test")
public class TestService {
    @GetMapping("test01")
    @Lock(value = "test01")
    public void test01() throws InterruptedException {
        Thread.sleep(5000);
        log.info(">test01", "");
    }
    @GetMapping("test02")
    @Lock(value = "{#name}", lockTimeoutSpel = "{#age}", lockWaitTimeSpel = "{#age2}", timeUnitSpel = "{#timeUnit}")
    public void test02(String name, Long age, Long age2, TimeUnit timeUnit) {
        log.info(">test02", "");
    }
    @GetMapping("test3")
    @Lock(value = "{#name}:{#age}")
    public void test03(String name, Integer age) {
        log.info(">test03", "");
    }

}
