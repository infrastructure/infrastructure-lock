package com.poizon.infrastructure.lock.starter.redisson.config;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.context.annotation.Bean;

@Slf4j
@ConditionalOnClass({RedissonConfig.class, RedissonClient.class})
public class RedissonConfig {
    @Value("${poizon.lock.redis.redisson.ip}")
    private String host;
    @Value("${poizon.lock.redis.redisson.port}")
    private String port;
    @Value("${poizon.lock.redis.redisson.db}")
    private Integer db;

    @Bean("lockRedissonClient")
    public RedissonClient redissonClient() {
        log.info(">初始化RedissonClient");
        Assert.notBlank(host);
        Assert.notBlank(port);
        Assert.notNull(db);

        Config config = new Config();
        config.useSingleServer()
                .setDatabase(db)
                .setAddress(
                        StrUtil.format(
                                "redis://{}:{}",
                                host,
                                port
                        )
                )
//                .setPassword("")
        ;
        return Redisson.create(config);
    }
}
