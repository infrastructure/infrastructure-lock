package com.poizon.infrastructure.lock.starter.redisson;

import com.poizon.infrastructure.lock.starter.redisson.config.RedissonConfig;
import com.poizon.infrastructure.lock.starter.redisson.config.RedissonLockConfig;
import org.springframework.context.annotation.Import;

@Import(value = {
        RedissonConfig.class,
        RedissonLockConfig.class
})
public class RedissonAutoConfig {
}
