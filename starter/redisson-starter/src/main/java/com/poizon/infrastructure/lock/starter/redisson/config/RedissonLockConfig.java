package com.poizon.infrastructure.lock.starter.redisson.config;

import com.poizon.infrastructure.lock.core.util.ILock;
import com.poizon.infrastructure.lock.starter.redisson.util.RedissonLock;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.context.annotation.Bean;

@Slf4j
@ConditionalOnBean(name = "lockRedissonClient", value = RedissonClient.class)
public class RedissonLockConfig {
    @Bean("redissonLock")
    public ILock redissonLock(@Qualifier(value = "lockRedissonClient") RedissonClient redissonClient) {
        log.info(">初始化RedissonLock");
        return new RedissonLock(redissonClient);
    }
}
