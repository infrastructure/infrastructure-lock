package com.poizon.infrastructure.lock.core.util;


import java.util.concurrent.TimeUnit;

public interface ILock {

    boolean tryLock(String lockName) throws InterruptedException;

    boolean tryLock(String lockName, Long timeout) throws InterruptedException;

    boolean tryLock(String lockName, Long timeout, TimeUnit timeUnit) throws InterruptedException;

    boolean tryLock(String lockName, Long waitTime, Long timeout, TimeUnit timeUnit) throws InterruptedException;

    void unlock(String lockName);

    void tryLock(String lockName, Runnable serviceExecutor) throws InterruptedException;

    void tryLock(String lockName, Long timeout, Runnable serviceExecutor) throws InterruptedException;

    void tryLock(String lockName, Runnable serviceExecutor, Runnable exceptionExecutor) throws InterruptedException;

    void tryLock(String lockName, Long timeout, Runnable serviceExecutor, Runnable exceptionExecutor) throws InterruptedException;

    void tryLock(String lockName, Long timeout, TimeUnit timeUnit, Runnable serviceExecutor, Runnable exceptionExecutor) throws InterruptedException;

    void tryLock(String lockName, Long waitTime, Long timeout, TimeUnit timeUnit, Runnable serviceExecutor, Runnable exceptionExecutor) throws InterruptedException;
}
