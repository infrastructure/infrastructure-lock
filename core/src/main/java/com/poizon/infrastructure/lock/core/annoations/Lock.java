package com.poizon.infrastructure.lock.core.annoations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.concurrent.TimeUnit;

/**
 * 分布式锁注解
 *
 * @author maofeiyu
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Lock {
    /**
     * 分布式key(支持spel表达式)
     */
    String value();

    /**
     * 锁等待时间
     */
    long lockWaitTime() default 1;

    String lockWaitTimeSpel() default "";


    /**
     * 锁超时时间
     */
    long lockTimeout() default 3;

    String lockTimeoutSpel() default "";

    TimeUnit timeUnit() default TimeUnit.SECONDS;

    String timeUnitSpel() default "";

}
