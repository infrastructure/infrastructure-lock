package com.poizon.infrastructure.lock;

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;

public class BannerPrint implements ApplicationRunner {

    @Override
    public void run(ApplicationArguments args) throws Exception {
        System.out.println(" ██                            ██\n" +
                " ██                            ██\n" +
                " ██                            ██\n" +
                " ██         ░████░     ▓████▒  ██  ▓██▒\n" +
                " ██        ░██████░   ███████  ██ ▓██▒\n" +
                " ██        ███  ███  ▓██▒  ▒█  ██▒██▒\n" +
                " ██        ██░  ░██  ██░       ████▓\n" +
                " ██        ██    ██  ██        █████\n" +
                " ██        ██░  ░██  ██░       ██░███\n" +
                " ██        ███  ███  ▓██▒  ░█  ██  ██▒\n" +
                " ████████  ░██████░   ███████  ██  ▒██\n" +
                " ████████   ░████░     ▓████▒  ██   ███\n");
    }
}
