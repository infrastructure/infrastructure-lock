package com.poizon.infrastructure.lock;

import com.poizon.infrastructure.lock.core.annoations.config.AopConfig;
import com.poizon.infrastructure.lock.core.enums.LockConstants;
import org.springframework.context.annotation.Import;

@Import(value = {
        BannerPrint.class,
        // 初始化锁配置常量
        LockConstants.class,
        // 初始化锁注解AOP
        AopConfig.class,
})
public class LockAutoConfig {


}
