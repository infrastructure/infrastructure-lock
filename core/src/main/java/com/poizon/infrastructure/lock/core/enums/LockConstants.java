package com.poizon.infrastructure.lock.core.enums;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁常量定义
 */
@Data
public class LockConstants {
    public static final String POIZON_LOCK_LOCK_DB_TYPE_ENUM = "poizon.lock.lockDbTypeEnum";
    public static final String POIZON_LOCK_TIME_UNIT = "poizon.lock.timeUnit";
    public static final String POIZON_LOCK_RETRY_COUNT = "poizon.lock.retryCount";
    public static final String POIZON_LOCK_WAIT_TIME = "poizon.lock.waitTime";
    public static final String POIZON_LOCK_TIMEOUT = "poizon.lock.timeout";
    public static final String POIZON_LOCK_PREFIX = "poizon.lock.prefix";
    public static String LOCK_NAME_PREFIX = "lock:";

    @Value("${" + POIZON_LOCK_PREFIX + ":lock:}")
    public void setLockNamePrefix(String lockNamePrefix) {
        LOCK_NAME_PREFIX = lockNamePrefix;
    }

    /**
     * 默认锁超时删除时间
     */
    public static Long DEFAULT_SERVICE_LOCK_TIMEOUT = 10L;

    @Value("${" + POIZON_LOCK_TIMEOUT + ":10}")
    public void setDefaultServiceLockTimeout(Long defaultServiceLockTimeout) {
        DEFAULT_SERVICE_LOCK_TIMEOUT = defaultServiceLockTimeout;
    }

    /**
     * 默认争抢锁等待时间
     */
    public static Long DEFAULT_WAIT_TIME = 3l;

    @Value("${" + POIZON_LOCK_WAIT_TIME + ":30}")
    public void setDefaultWaitTime(Long defaultWaitTime) {
        DEFAULT_WAIT_TIME = defaultWaitTime;
    }

    /**
     * 锁重试次数
     */
    public static Integer DEFAULT_RETRY_COUNT = 5;

    @Value("${" + POIZON_LOCK_RETRY_COUNT + ":5}")
    public void setDefaultMaxRetryCount(Integer defaultMaxRetryCount) {
        DEFAULT_RETRY_COUNT = defaultMaxRetryCount;
    }

    /**
     * 默认时间单位
     */
    public static TimeUnit DEFAULT_TIME_UNIT = TimeUnit.SECONDS;

    @Value("${" + POIZON_LOCK_TIME_UNIT + ":SECONDS}")
    public void setDefaultTimeUnit(TimeUnit defaultTimeUnit) {
        DEFAULT_TIME_UNIT = defaultTimeUnit;
    }

    public static String lockDbTypeEnum = "redissonLock";

    @Value("${" + POIZON_LOCK_LOCK_DB_TYPE_ENUM + ":redissonLock}")
    public void setDefaultTimeUnit(String defaultTimeUnit) {
        lockDbTypeEnum = defaultTimeUnit;
    }

}
